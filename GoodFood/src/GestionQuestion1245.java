import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JLabel;
import javax.swing.JTextField;

public class GestionQuestion1245 implements ActionListener {

	private JTextField date1;
	private JTextField date2;
	private Modele m; 
	private int question;
		
	public GestionQuestion1245(JTextField d1, JTextField d2, Modele mod, int nbQuestion) {
		date1 = d1;
		date2 = d2;
		m = mod;	
		question = nbQuestion;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		String[] d1 = date1.getText().split("/");
		String[] d2 = date2.getText().split("/");
		
		if(d1.length == 3 && d2.length == 3 && VerificateurDeDates.etreDateValide(d1[0],d1[1],d1[2]) && VerificateurDeDates.etreDateValide(d2[0],d2[1],d2[2])) {
			try {
				switch(question) {
				case 1:					
					m.q1(date1.getText(),date2.getText());
					break;
				case 2:
					m.q2(date1.getText(),date2.getText());
					break;
				case 4:
					m.q4(date1.getText(),date2.getText());
					break;
				case 5:
					m.q5(date1.getText(),date2.getText());
					break;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			m.afficherErreur();
		}
	}
	
	
}
