import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Observable;

public class Modele extends Observable {
	
	private String url;
	private Connection cnt;
	
	private String resultats;
	
	public String getResultats() {
		return resultats;
	}

	public Modele(Connection connex, String l) {
		url = l;
		cnt = connex;
		resultats = "";
	}
	
	public void q1(String str1, String str2) throws SQLException {
		PreparedStatement stt = cnt.prepareStatement("SELECT Distinct numplat, libelle FROM plat natural join contient natural join commande WHERE datcom BETWEEN ? AND ?");

		stt.setString(1,str1);
		stt.setString(2,str2);
		ResultSet rs = stt.executeQuery();
		
		resultats = "<html>";
		
		while (rs.next()) {
			int numplat = rs.getInt(1);
			String libelle = rs.getString(2);
			resultats = numplat + " : " + libelle + "<br/>";
		}

		resultats += "</html>";
		setChanged();
		notifyObservers();

		stt.close();
		rs.close();

	}

	public void q2(String str1, String str2) throws SQLException {
		PreparedStatement stt = cnt.prepareStatement("SELECT Distinct numplat, libelle FROM plat natural join contient natural join commande WHERE not (datcom BETWEEN ? AND ?)");

		stt.setString(1,str1);
		stt.setString(2,str2);
		ResultSet rs = stt.executeQuery();
		
		resultats = "<html>";

		while (rs.next()) {
			int numplat = rs.getInt(1);
			String libelle = rs.getString(2);
			resultats = numplat + " : " + libelle + "<br/>";
		}

		resultats += "</html>";
		setChanged();
		notifyObservers();
		
		stt.close();
		rs.close();

	}

	public void q3(String str1, String str2, int numtab) throws SQLException {
		PreparedStatement stt = cnt.prepareStatement("SELECT Distinct nomserv, dataff libelle FROM affecter natural join serveur WHERE dataff BETWEEN ? AND ? and numtab = ?");

		stt.setString(1,str1);
		stt.setString(2,str2);
		stt.setInt(3, numtab);
		ResultSet rs = stt.executeQuery();

		resultats = "<html>";
		
		while (rs.next()) {
			String nom = rs.getString(1);
			String date = rs.getString(2);
			resultats = nom + " : " + date + "<br/>";
		}

		resultats = "</html>";
		setChanged();
		notifyObservers();
		
		stt.close();
		rs.close();  
	}

	public void q4(String str1, String str2) throws SQLException {
		PreparedStatement stt = cnt.prepareStatement("SELECT numserv, Sum(prixunit*quantite) as CA, count(distinct numcom) as nbCommande FROM serveur natural join commande natural join affecter natural join contient natural join plat WHERE datcom BETWEEN ? AND ? GROUP BY numserv ORDER BY CA DESC");

		stt.setString(1,str1);
		stt.setString(2,str2);
		ResultSet rs = stt.executeQuery();
		
		resultats = "<html>";
		
		while (rs.next()) {
			String numserv = rs.getString(1);
			int ca = rs.getInt(2);
			int nbcom = rs.getInt(3);
			resultats = numserv + " : " + ca + " : " + nbcom + "<br/>";
		}

		resultats = "</html>";
		setChanged();
		notifyObservers();

		stt.close();
		rs.close();  
	}

	public void q5(String str1, String str2) throws SQLException {
		PreparedStatement stt = cnt.prepareStatement("SELECT numserv FROM serveur WHERE numserv NOT IN (SELECT numserv FROM serveur natural join commande natural join affecter natural join contient natural join plat WHERE datcom BETWEEN ? AND ? GROUP BY numserv HAVING Sum(quantite*prixunit) > 0)");

		stt.setString(1,str1);
		stt.setString(2,str2);
		ResultSet rs = stt.executeQuery();

		resultats = "<html>";
		
		while (rs.next()) {
			String numserv = rs.getString(1);
			resultats = numserv + "<br/>";
		}

		resultats = "</html>";
		setChanged();
		notifyObservers();

		stt.close();
		rs.close();  
	}

	public void q6(int commande) throws SQLException {
		PreparedStatement stt = cnt.prepareStatement("SELECT Sum(quantite*prixunit) as prix FROM commande natural join contient natural join plat WHERE numcom = ?");
		PreparedStatement stt2 = cnt.prepareStatement("UPDATE commande SET montcom =  ? WHERE numcom = ?");
		
		stt.setInt(1,commande);
		ResultSet rs = stt.executeQuery();

		while (rs.next()) {
			int total = rs.getInt(1);
			stt2.setInt(1,total);
			stt2.setInt(2, commande);
			ResultSet rs2 = stt2.executeQuery();
			System.out.println("Prix de la commande actualis� !\n");
		}

		stt.close();
		rs.close();  
	}
	
	public void afficherErreur() {
		resultats = "Format non valide (le format doit etre 'JJ/MM/YYYY' par exemple '15/08/2012')";
		setChanged();
		notifyObservers();
	}
	
}
