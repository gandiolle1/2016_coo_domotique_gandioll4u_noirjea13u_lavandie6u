import java.sql.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class Main {

	public static void main(String[] args) {
				
		//try {
			
			JFrame fenetre = new JFrame("Menu principal");
			Main.Menu(fenetre);
			//cnt.close();
		//} catch (SQLException e) {
			//e.printStackTrace();
		//}
		
		
		/*try {
			Main m = new Main();

			m.q1("10/10/2010","10/10/2020");
			m.q2("10/10/2010","10/10/2015");
			m.q3("10/10/2010","10/10/2020", 17);
			m.q4("10/10/2010","10/10/2020");
			m.q5("10/10/2010","10/10/2020");
			m.q6(106);

			m.cnt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}*/
	}
	
	public static void Menu(JFrame fenetre) {
		
		/*String url = "jdbc:oracle:thin:@charlemagne:1521:infodb";
		try {
			Connection cnt = DriverManager.getConnection(url, "lavandie6u", "Medeni(1");
		} catch (SQLException e) {
			e.printStackTrace();
		}*/
		
		Modele mod = new Modele(null,null);
		fenetre.setSize(new Dimension(400,400));
		JButton b1,b2,b3,b4,b5,b6,b7;
		
		JPanel p = new JPanel();
		
		b1 = new JButton("Question 1");
		b1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main.question1245(fenetre,mod,1);
			}
			
		});
		
		b2 = new JButton("Question 2");
		b2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main.question1245(fenetre,mod,2);
			}
			
		});
		
		b3 = new JButton("Question 3");
		b3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main.Question3(fenetre,mod);
			}
			
		});
		
		b4 = new JButton("Question 4");
		b4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main.question1245(fenetre,mod,4);
			}
			
		});
		
		b5 = new JButton("Question 5");
		b5.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main.question1245(fenetre,mod,5);
			}
			
		});
		
		b6 = new JButton("Question 6");
		b7 = new JButton("Question 7");
		
		JButton[] boutons = {b1,b2,b3,b4,b5,b6,b7};
		
		GridLayout gL = new GridLayout(4,2,5,5);
		p.setLayout(gL);
	
		for(JButton b : boutons) {
			b.setFont(new Font("SansSerif",Font.BOLD,15));
			b.setBackground(new Color(255,200,87));
			b.setForeground(Color.BLACK);
			b.setFocusPainted(false);
			b.setBorderPainted(false);
			
			b.setCursor(new Cursor(Cursor.HAND_CURSOR));
			
			p.add(b);
		}
		
		p.setPreferredSize(new Dimension(400,400));
		p.setBackground(new Color(75,63,114));

		
		fenetre.setContentPane(p);
		fenetre.pack();
		fenetre.setVisible(true);
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	public static void question1245(JFrame fenetre, Modele mod, int nbQuestion) {
		
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));
		
		fenetre.setSize(new Dimension(600,400));

		String q = "";
		switch(nbQuestion) {
		case 1:					
			q = "Liste des plats qui ont ete commandes pendant cette periode";
			break;
		case 2:			
			q = "Liste des plats qui ont ete commandes pendant cette periode";
			break;
		case 4:
			q = "Chiffre d'affaire et nombre de commandes realisees par chaque serveur pendant cette periode";
			fenetre.setSize(new Dimension(800,400));
			break;
		case 5:
			q = "Serveurs n'ayant pas realise de chiffre d'affaire pendant cette periode";
			fenetre.setSize(new Dimension(800,400));
			break;
		}
		
		JLabel question = new JLabel(q);
		question.setFont(new Font("SansSerif",Font.BOLD,15));
		
		JPanel milieu = new JPanel();
		JLabel debut = new JLabel("Du");
		JTextField date1 = new JTextField("12/08/2001");
		JLabel au = new JLabel("au");
		
		JTextField date2 = new JTextField("12/08/2018");
		
		JButton valider = new JButton("Valider");
		
		JButton quit = new JButton("Menu");
		quit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				Main.Menu(fenetre);
				
			}
			
		});
		
		milieu.add(debut);
		milieu.add(date1);
		milieu.add(au);
		milieu.add(date2);
		milieu.add(valider);
		milieu.add(quit);
		
		VueResultats resultats = new VueResultats();
		
		mod.addObserver(resultats);
		valider.addActionListener(new GestionQuestion1245(date1,date2,mod,nbQuestion));
		
		panel.add(question);
		panel.add(milieu);
		panel.add(resultats);
		
		fenetre.setContentPane(panel);
		fenetre.setVisible(true);
		
	
	}
	
	public static void Question3(JFrame fenetre, Modele mod) {
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));

		JLabel question = new JLabel("Liste des serveurs qui ont servit cette table pendant cette periode");
		question.setFont(new Font("SansSerif",Font.BOLD,15));
		JPanel milieu = new JPanel();
		
		JLabel tab = new JLabel("Table numero ");
		JTextField numeroTable = new JTextField("15");
		JLabel du = new JLabel(" du");
		JTextField date1 = new JTextField("12/08/2001");
		JLabel au = new JLabel("au");
		JTextField date2 = new JTextField("12/08/2018");
		
		JButton valider = new JButton("Valider");
		
		JButton quit = new JButton("Menu");
		quit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				Main.Menu(fenetre);
				
			}
			
		});
		
		fenetre.setSize(new Dimension(800,400));

		milieu.add(tab);
		milieu.add(numeroTable);
		milieu.add(du);
		milieu.add(date1);
		milieu.add(au);
		milieu.add(date2);
		milieu.add(valider);
		milieu.add(quit);
		
		VueResultats resultats = new VueResultats();
		
		mod.addObserver(resultats);
		valider.addActionListener(new GestionQuestion3(date1,date2,mod,numeroTable));
		
		panel.add(question);
		panel.add(milieu);
		panel.add(resultats);
		
		fenetre.setContentPane(panel);
		fenetre.setVisible(true);
		
		
	}
	

}
