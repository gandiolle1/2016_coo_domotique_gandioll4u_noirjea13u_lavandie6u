import java.awt.Color;
import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;

public class VueResultats extends JTextPane implements Observer {
	
	@Override
	public void update(Observable arg0, Object arg1) {
		this.setText(((Modele)arg0).getResultats());
	}
	
	
	
	
}
