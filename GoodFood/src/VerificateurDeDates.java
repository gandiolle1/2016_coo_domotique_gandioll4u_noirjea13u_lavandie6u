import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class VerificateurDeDates {
	
public static boolean etreDateValide(String jour, String mois, String annee) {
		
	
	    jour = jour.length() <= 1 ? '0' + jour : jour;
	    mois = mois.length() <= 1 ? '0' + mois : mois;
	    	
		String date = annee + mois + jour;
		
		DateTimeFormatter format = DateTimeFormatter.BASIC_ISO_DATE;
		
		boolean dateCorrecte = true;
		
		try {
			format.parse(date);
		} catch (DateTimeParseException e) {
			dateCorrecte = false;
		}
		
		return dateCorrecte;
		
	}

}
