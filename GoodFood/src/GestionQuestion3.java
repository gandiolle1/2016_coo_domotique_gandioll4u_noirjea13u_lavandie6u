import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JTextField;

public class GestionQuestion3 implements ActionListener {
	
	private JTextField date1,date2,table;
	private Modele m; 
		
	public GestionQuestion3(JTextField d1, JTextField d2, Modele mod, JTextField tab) {
		date1 = d1;
		date2 = d2;
		m = mod;	
		table = tab;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		String[] d1 = date1.getText().split("/");
		String[] d2 = date2.getText().split("/");
		
		if(Integer.parseInt(table.getText()) >= 0 && d1.length == 3 && d2.length == 3 && VerificateurDeDates.etreDateValide(d1[0],d1[1],d1[2]) && VerificateurDeDates.etreDateValide(d2[0],d2[1],d2[2])) {
			try {
				m.q3(date1.getText(),date2.getText(),Integer.parseInt(table.getText()));
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} else {
			m.afficherErreur();
		}
	}

}
