package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import donnees.*;

import org.junit.Test;

public class TestMagasin {

	@Test
	public void testTrierArtiste() {
		Magasin m = new Magasin();
		CD c1 = new CD("toto1","1");
		CD c2 = new CD("toto2","2");
		CD c3 = new CD("toto3","3");
		ArrayList<CD> l = new ArrayList<CD>();
		l.add(c1);
		l.add(c2);
		l.add(c3);
		
		m.ajouteCd(c2);
		m.ajouteCd(c1);
		m.ajouteCd(c3);
		
		ComparateurArtiste ca = new ComparateurArtiste();
		m.trier(ca);
		
		for(int i = 0;i < 3;i++) {
			assertEquals("on devrait avoir le CD" + i,l.get(i),m.getCd(i));
		}
	}
	
	@Test
	public void testTrierAlbum() {
		Magasin m = new Magasin();
		CD c1 = new CD("toto1","1");
		CD c2 = new CD("toto2","2");
		CD c3 = new CD("toto3","3");
		ArrayList<CD> l = new ArrayList<CD>();
		l.add(c1);
		l.add(c2);
		l.add(c3);
		
		m.ajouteCd(c2);
		m.ajouteCd(c1);
		m.ajouteCd(c3);
		
		ComparateurAlbum ca = new ComparateurAlbum();
		m.trier(ca);
		
		for(int i = 0;i < 3;i++) {
			assertEquals("on devrait avoir le CD" + i,l.get(i),m.getCd(i));
		}
	}
	
	@Test
	public void testRechercherArtiste() {
		Magasin m = new Magasin();
		CD c1 = new CD("toto1","1");
		CD c2 = new CD("toto2","2");
		CD c3 = new CD("toto3","3");
		
		m.ajouteCd(c2);
		m.ajouteCd(c1);
		m.ajouteCd(c3);
		
		SelecteurArtiste sa = new SelecteurArtiste("toto1");
		ArrayList<CD> l = m.chercher(sa);

		assertEquals("on devrait avoir le CD c1",c1,l.get(0));
	}
	@Test
	public void testRechercherNombrePiste() {
		Magasin m = new Magasin();
		CD c1 = new CD("toto1","1");
		CD c2 = new CD("toto2","2");
		CD c3 = new CD("toto3","3");
		
		c1.ajouterPiste(new InfoPiste("piste",10));
		
		m.ajouteCd(c2);
		m.ajouteCd(c1);
		m.ajouteCd(c3);
		
		SelecteurNombrePiste sa = new SelecteurNombrePiste(1);
		ArrayList<CD> l = m.chercher(sa);

		assertEquals("on devrait avoir le CD c1",c1,l.get(0));
	}

}
