package test;

import static org.junit.Assert.*;
import java.io.FileNotFoundException;
import org.junit.Test;

import donnees.*;
import XML.*;

public class TestChargeurMagasin {

	@Test
	public void testChargerMagasin() {
		ChargeurMagasin cm = new ChargeurMagasin("musicbrainzSimple");
		try {
			Magasin m = cm.chargerMagasin();	
			assertEquals("on doit recuperer le magasin",12,m.getNombreCds());
		}catch (FileNotFoundException e) {}

	}
	
	@Test (expected = FileNotFoundException.class)
	public void testChargerMagasinRepertoireInexistant() throws FileNotFoundException {
		ChargeurMagasin cm = new ChargeurMagasin(" ");
		Magasin m = cm.chargerMagasin();
	}

}
