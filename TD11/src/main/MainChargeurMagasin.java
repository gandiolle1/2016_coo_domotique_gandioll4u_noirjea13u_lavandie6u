package main;

import java.io.IOException;
import java.util.Scanner;

import donnees.*;
import XML.ChargeurMagasin;

/**
 * permet de charger un magasin de test
 */
public class MainChargeurMagasin {

	/**
	 * methode principale
	 * 
	 * @param args
	 *            inutilise
	 * @throws IOException
	 *             en cas de probleme de lecture entree/sortie
	 */
	public static void main(String args[]) throws IOException {
		
		String repertoire = "src/musicbrainzSimple/";
		ChargeurMagasin charge = new ChargeurMagasin(repertoire);
		Magasin resultat = charge.chargerMagasin();
		System.out.println(resultat);

		Scanner sc = new Scanner(System.in);
		sc.nextLine();
		sc.close();

		ComparateurArtiste ca1 = new ComparateurArtiste();
		resultat.trier(ca1);
		System.out.println(resultat);

		ComparateurAlbum ca2 = new ComparateurAlbum();
		resultat.trier(ca2);
		System.out.println(resultat);

	}

}
