package donnees;

public class SelecteurNombrePiste implements Selecteur {
		
		private int nbPiste;
		
		public SelecteurNombrePiste(int nbPistes) {
			nbPiste = nbPistes;
		}
		
		@Override
		public boolean garderCd(CD cd) {
			return cd.getPistes().size() == nbPiste;
		}
		
}
