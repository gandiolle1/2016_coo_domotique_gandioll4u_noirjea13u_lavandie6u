package donnees;

public class ComparateurAlbum implements ComparateurCd{

	@Override
	public boolean comparer(CD c1, CD c2) {
		String album1 = c1.getNomCD();
		String album2 = c2.getNomCD();
		return album1.compareTo(album2) > 0;
	}

}
