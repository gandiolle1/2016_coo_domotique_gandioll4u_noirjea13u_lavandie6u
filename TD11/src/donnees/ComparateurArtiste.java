package donnees;

public class ComparateurArtiste implements ComparateurCd{

	@Override
	public boolean comparer(CD c1, CD c2) {
		String artiste1 = c1.getNomArtiste();
		String artiste2 = c2.getNomArtiste();
		return artiste1.compareTo(artiste2) > 0;
	}	
}
