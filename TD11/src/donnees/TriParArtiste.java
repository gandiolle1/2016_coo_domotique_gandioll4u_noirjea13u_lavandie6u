package donnees;

import java.util.Comparator;

public class TriParArtiste implements Comparator<CD> {

	@Override
	public int compare(CD o1, CD o2) {
		return o1.getNomArtiste().compareTo(o2.getNomArtiste());
	}


}
