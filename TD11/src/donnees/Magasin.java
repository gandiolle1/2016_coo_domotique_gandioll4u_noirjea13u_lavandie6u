package donnees;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * La classe Magasin represente un magasin qui vend des CDs.</p>
 *
 * cette classe est caracterisee par un ensemble de CDs correspondant aux CDS
 * vendus dans ce magasin.
 *
 */
public class Magasin {

	/**
	 * la liste des CDs disponibles en magasin
	 */
	private ArrayList<CD> listeCds;

	/**
	 * construit un magasin par defaut qui ne contient pas de CD
	 */
	public Magasin() {
		listeCds = new ArrayList<CD>();
	}

	/**
	 * ajoute un cd au magasin
	 *
	 * @param cdAAjouter
	 *            le cd a ajouter
	 */
	public void ajouteCd(CD cdAAjouter) {
		listeCds.add(cdAAjouter);
	}

	@Override
	/**
	 * affiche le contenu du magasin
	 */
	public String toString() {
		String chaineResultat = "";
		//parcours des Cds
		for (int i = 0; i < listeCds.size(); i++) {
			chaineResultat += listeCds.get(i);
		}
		chaineResultat += "nb Cds: " + listeCds.size();
		return (chaineResultat);

	}

	/**
	 * @return le nombre de Cds du magasin
	 */
	public int getNombreCds() {
		return listeCds.size();
	}

	/**
	 * permet d'acceder � un CD
	 *
	 * @return le cd a l'indice i ou null si indice est non valide
	 */
	public CD getCd(int i)
	{
		CD res=null;
		if ((i>=0)&&(i<this.listeCds.size()))
			res=this.listeCds.get(i);
		return(res);
	}


	public void trier(ComparateurCd tri) {
		ArrayList<CD> triAlbum = new ArrayList<CD>();

		while(listeCds.size() != 0) {
			
			CD comp = listeCds.get(0);
			int sauv = 0;

			for(int i = 0; i < listeCds.size(); i++) {
				boolean res = tri.comparer(comp,listeCds.get(i));
				if(res) {
					comp = listeCds.get(i);
					sauv = i;
				}
			}
			
			triAlbum.add(comp);
			listeCds.remove(sauv);
		}

		listeCds = triAlbum;

	}
	
	public ArrayList<CD> chercher(Selecteur selec) {
		
		int i =0;
		ArrayList<CD> liste = new ArrayList<CD>();
		
		while(listeCds.size() != i) {
			if(selec.garderCd(listeCds.get(i))) {
				liste.add(listeCds.get(i));
			}
			i = i +1;
		}
		
		return liste;
	}



	// TODO  ajouter une methode de tri

}
