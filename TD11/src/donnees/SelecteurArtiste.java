package donnees;

public class SelecteurArtiste implements Selecteur {
	
	private String nomArtiste;
	
	public SelecteurArtiste(String nom) {
		nomArtiste = nom;
	}
	
	@Override
	public boolean garderCd(CD cd) {
		return cd.getNomArtiste() == nomArtiste;
	}
	
}
