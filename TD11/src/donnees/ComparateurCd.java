package donnees;

public interface ComparateurCd {

	boolean comparer(CD c1, CD c2);

}
